(define (p r c) (if (or (= c 1) (= r c)) 1 (+ (p (- r 1) (- c 1)) (p (- r 1) c ))))
