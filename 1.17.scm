(define (even? n)
(= (remainder n 2) 0))

(define (double x) (* x 2))
(define (halve x) (/ x 2))

(define (fast-* a b) (
  cond 
    ((= b 1) a)
    ((even? b) (fast-* (double a) (halve b)))
    (else (+ a (fast-* a (- b 1))))
))