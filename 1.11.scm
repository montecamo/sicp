
(define (f n) (if (< n 3) n (+ (f (- n 1)) (f (- n 2)) (f (- n 3))))) // recursive

(define (f-iter a b c count) (if (= count 0) a (f-iter b c (+ a b c) (- count 1))))

(define (f n) (f-iter 0 1 2 n)) // iterative

