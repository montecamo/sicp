(define (even? n)
(= (remainder n 2) 0))

(define (double x) (* x 2))
(define (halve x) (/ x 2))

(define (fast-*-iter a b product) (
  cond 
    ((= b 0) product)
    ((even? b) (fast-*-iter (double a) (halve b) product))
    (else (fast-*-iter a (- b 1) (+ product a)))
))

(define (fast-* a b) (fast-*-iter a b 0))