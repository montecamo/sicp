(define (square x) (* x x))

(define (even? n)
(= (remainder n 2) 0))

(define (expt-iter x n product) (
  cond
    ((= n 0) product)
    ((even? n) (expt-iter (square x) (/ n 2) product))
    (else (expt-iter x (- n 1) (* product x)))
))

(define (fast-expt x n) (
  expt-iter x n 1
))

(define (sqrt-iter x n product) (if (= n 0) product (sqrt-iter x (- n 1) (* product x))))
